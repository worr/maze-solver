.globl read_input
.globl print_maze
.globl find_start
#.globl push - DEBUGGING PURPOSES
#.globl pop - DEBUGGING PURPOSES
#.globl peek - DEBUGGING PURPOSES
.globl check_direction
.globl check_decision
.globl move_mouse
.globl backtrack
.globl check_end
.globl copy_maze
.globl finish_solved

.globl main

LEFT = -1
RIGHT = 1
PRINT_INT = 1
PRINT_STRING = 4
EXIT = 10

.data
size: 
.word 0
.word 0
.word 0

maze:
.space 80*80

solved:
.space 80*80

greetz:
.asciiz "===\n=== Maze Solver\n=== by\n=== William Orr\n===\n\n\nInput Maze:\n\n"

solution_text:
.asciiz "\n\nSolution:\n\n"

start_loc:
.word 0

current_loc:
.word 0

newline:
.asciiz "\n"

.text
main: 
    la $a0, size
    la $a1, maze
    jal read_input

    la $a0, greetz
    li $v0, PRINT_STRING
    syscall

    la $a0, size
    la $a1, maze
    jal print_maze

    la $a1, solved
    la $a0, maze
    jal copy_maze

# DEBUG CODE - leaving this in here just for reference
    #jal debug_sizes
    #jal debug_check_direction
    #jal debug_check_decision
    #jal debug_move # Works with sample1.in
    #jal debug_backtrack # Works with test1.in
    #jal debug_check_end # Works with test2.in

# Let's write our main function
# Need to set start_loc
    la $a0, size
    la $a1, maze
    la $a2, start_loc
    jal find_start 

# Need to copy start_loc to current_loc
    la $t0, start_loc
    lw $t1, 0($t0)
    la $t0, current_loc
    sw $t1, 0($t0)

# Initial direction is left
    li $s0, LEFT

main_loop_start:
# Check to see if we hit the end and jump if we do
    la $a0, current_loc
    la $t0, size
    lw $a1, 8($t0)
    jal check_end
    bne $v0, $zero, main_loop_end

# Check to see if current direction is still unblocked
    la $a0, current_loc
    move $a1, $s0
    jal check_direction
    beq $v0, $zero, main_loop_cycle_directions
    lw $a1, 0($a0)
    add $a1, $a1, $s0
    jal move_mouse
    j main_loop_start

main_loop_cycle_directions:
# Check left
    la $a0, current_loc
    li $a1, LEFT
    jal check_direction
    bne $v0, $zero, main_loop_cycle_directions_cleanup

# Check right
    la $a0, current_loc
    li $a1, RIGHT
    jal check_direction
    bne $v0, $zero, main_loop_cycle_directions_cleanup

# Check up
    la $a0, current_loc
    la $t0, size
    lw $a1, 8($t0)
    sub $a1, $zero, $a1
    jal check_direction
    bne $v0, $zero, main_loop_cycle_directions_cleanup

# Check down
    la $a0, current_loc
    la $t0, size
    lw $a1, 8($t0)
    jal check_direction
    bne $v0, $zero, main_loop_cycle_directions_cleanup

# If we made it here, we need to backtrack
    la $a0, current_loc
    la $t0, size
    lw $a1, 8($t0)
    jal backtrack
    j main_loop_start

main_loop_cycle_directions_cleanup:
    move $s0, $a1
    j main_loop_start

main_loop_end:

main_print_solution:
    la $a0, maze
    la $a1, solved
    jal finish_solved

    la $a0, solution_text
    li $v0, PRINT_STRING
    syscall

    la $a0, size
    la $a1, solved
    jal print_maze

    li $v0, EXIT
    syscall

debug_check_direction:
    addi $sp, $sp, -4
    sw $ra, 0($sp)

    la $a0, size
    la $a1, maze
    la $a2, start_loc
    jal find_start 

# Check left
    la $a0, start_loc
    li $a1, -1
    jal check_direction
    move $a0, $v0
    li $v0, PRINT_INT
    syscall
    la $a0, newline
    li $v0, PRINT_STRING
    syscall

# Check right
    la $a0, start_loc
    li $a1, 1
    jal check_direction
    move $a0, $v0
    li $v0, PRINT_INT
    syscall
    la $a0, newline
    li $v0, PRINT_STRING
    syscall

# Check up
    la $a0, start_loc
    la $t0, size
    lw $a1, 8($t0)
    li $t0, -1
    mul $a1, $a1, $t0
    jal check_direction
    move $a0, $v0
    li $v0, PRINT_INT
    syscall
    la $a0, newline
    li $v0, PRINT_STRING
    syscall

# Check down
    la $a0, start_loc
    la $t0, size
    lw $a1, 8($t0)
    jal check_direction
    move $a0, $v0
    li $v0, PRINT_INT
    syscall
    la $a0, newline
    li $v0, PRINT_STRING
    syscall

    lw $ra, 0($sp)
    addi $sp, $sp, 4
    jr $ra

debug_sizes:
    addi $sp, $sp, -4
    sw $ra, 0($sp)

    la $t0, size
    lw $a0, 0($t0)
    li $v0, PRINT_INT
    syscall
    la $a0, newline
    li $v0, PRINT_STRING
    syscall

    lw $a0, 4($t0)
    li $v0, PRINT_INT
    syscall
    la $a0, newline
    li $v0, PRINT_STRING
    syscall

    lw $a0, 8($t0)
    li $v0, PRINT_INT
    syscall
    la $a0, newline
    li $v0, PRINT_STRING
    syscall

    lw $ra, 0($sp)
    addi $sp, $sp, 4
    jr $ra

debug_check_decision:
    addi $sp, $sp, -4
    sw $ra, 0($sp)

# We always use the start cause it's easy
    la $a0, start_loc
    la $t0, size
    lw $a1, 8($t0)
    jal check_decision
    move $a0, $v0

    li $v0, PRINT_INT
    syscall
    la $a0, newline
    li $v0, PRINT_STRING
    syscall

    lw $ra, 0($sp)
    addi $sp, $sp, 4

    jr $ra

# We're just gonna try moving right from the start
debug_move:
    addi $sp, $sp, -4
    sw $ra, 0($sp)

    la $t0, start_loc
    lw $a1, 0($t0)
    addi $a1, $a1, 1
    la $a0, current_loc
    jal move_mouse

    la $a0, current_loc
    lw $a1, 0($a0)
    addi $a1, $a1, 1
    jal move_mouse

    la $a0, size
    la $a1, maze
    jal print_maze

    lw $ra, 0($sp)
    addi $sp, $sp, 4

    jr $ra

# This is going to be terrible
debug_backtrack:
    addi $sp, $sp, -4
    sw $ra, 0($sp)

    la $t0, start_loc
    lw $a1, 0($t0)
    addi $a1, $a1, -1
    la $a0, current_loc
    jal move_mouse

    la $a0, current_loc
    lw $a1, 0($a0)
    addi $a1, $a1, -1
    jal move_mouse

    la $a0, current_loc
    la $t0, size
    lw $a1, 8($t0)
    jal backtrack

    la $a0, current_loc
    lw $a1, 0($a0)
    addi $a1, $a1, 7
    jal move_mouse

    la $a0, size
    la $a1, maze
    jal print_maze

    lw $ra, 0($sp)
    addi $sp, $sp, 4

    jr $ra

debug_check_end:
    addi $sp, $sp, -4
    sw $ra, 0($sp)

    la $a0, start_loc
    la $t0, size
    lw $a1, 8($t0)
    jal check_end

    move $a0, $v0
    li $v0, PRINT_INT
    syscall

    la $a0, newline
    li $v0, PRINT_STRING
    syscall

    lw $ra, 0($sp)
    addi $sp, $sp, 4

    jr $ra
