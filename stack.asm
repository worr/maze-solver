.globl push
.globl pop
#.globl peek

.data

stack_data:
.space 100*4 # Leave room for 100 moves on the stack

stack_head:
.word stack_data

.text

# Push move onto move stack
# $a0 - address of move
push:
    addi $sp, $sp, -36
    sw $ra, 32($sp)
    sw $s7, 28($sp)
    sw $s6, 24($sp)
    sw $s5, 20($sp)
    sw $s4, 16($sp)
    sw $s3, 12($sp)
    sw $s2, 8($sp)
    sw $s1, 4($sp)
    sw $s0, 0($sp)

    la $s0, stack_head
    lw $s1, 0($s0)
    addi $s1, $s1, 4
    sw $s1, 0($s0)

    lw $s0, 0($a0)
    sw $s0, 0($s1)

    lw $ra, 32($sp)
    lw $s7, 28($sp)
    lw $s6, 24($sp)
    lw $s5, 20($sp)
    lw $s4, 16($sp)
    lw $s3, 12($sp)
    lw $s2, 8($sp)
    lw $s1, 4($sp)
    lw $s0, 0($sp)
    addi $sp, $sp, 36

    jr $ra

# Pop the most recent move off of the move stack
# $a0 - address to store move information
pop:
    addi $sp, $sp, -36
    sw $ra, 32($sp)
    sw $s7, 28($sp)
    sw $s6, 24($sp)
    sw $s5, 20($sp)
    sw $s4, 16($sp)
    sw $s3, 12($sp)
    sw $s2, 8($sp)
    sw $s1, 4($sp)
    sw $s0, 0($sp)

    jal peek
    la $s0, stack_head
    lw $s1, 0($s0)
    addi $s1, $s1, -4
    sw $s1, 0($s0)

    lw $ra, 32($sp)
    lw $s7, 28($sp)
    lw $s6, 24($sp)
    lw $s5, 20($sp)
    lw $s4, 16($sp)
    lw $s3, 12($sp)
    lw $s2, 8($sp)
    lw $s1, 4($sp)
    lw $s0, 0($sp)
    addi $sp, $sp, 36

    jr $ra

# Peek at the top value on the move stack
# $a0 - address to store result
peek:
    addi $sp, $sp, -36
    sw $ra, 32($sp)
    sw $s7, 28($sp)
    sw $s6, 24($sp)
    sw $s5, 20($sp)
    sw $s4, 16($sp)
    sw $s3, 12($sp)
    sw $s2, 8($sp)
    sw $s1, 4($sp)
    sw $s0, 0($sp)

    la $s0, stack_head
    lw $s1, 0($s0)

    lw $t0, 0($s1)
    sw $t0, 0($a0)

    lw $ra, 32($sp)
    lw $s7, 28($sp)
    lw $s6, 24($sp)
    lw $s5, 20($sp)
    lw $s4, 16($sp)
    lw $s3, 12($sp)
    lw $s2, 8($sp)
    lw $s1, 4($sp)
    lw $s0, 0($sp)
    addi $sp, $sp, 36

    jr $ra
