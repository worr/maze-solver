#
# Makefile for CompOrg Experiment 3 - tree
#

#
# Location of the processing programs
#
RASM  = /home/fac/wrc/bin/rasm
RLINK = /home/fac/wrc/bin/rlink
RSIM  = /home/fac/wrc/bin/rsim

#
# Suffixes to be used or created
#
.SUFFIXES:	.asm .obj .lst .out

#
# Object files to be created
#
OBJECTS = maze.obj input.obj output.obj util.obj stack.obj movement.obj

#
# Transformation rule: .asm into .obj
#
.asm.obj:
	$(RASM) -l $*.asm > $*.lst

#
# Transformation rule: .obj into .out
#
.obj.out:
	$(RLINK) -o $*.out $*.obj

#
# Main target
#
main.out:	$(OBJECTS)
	$(RLINK) -m -o main.out $(OBJECTS) > main.map

run:	main.out
	$(RSIM) main.out

clean:
	rm *.obj *.lst *.map *.out
