.globl print_maze
PRINT_STRING = 4
.text

# This takes two arguments 
# $a0 - pointer to size
# $a1 - pointer to maze
print_maze:
    addi $sp, $sp, -36
    sw $ra, 32($sp)
    sw $s7, 28($sp)
    sw $s6, 24($sp)
    sw $s5, 20($sp)
    sw $s4, 16($sp)
    sw $s3, 12($sp)
    sw $s2, 8($sp)
    sw $s1, 4($sp)
    sw $s0, 0($sp)

    lw $s0, 0($a0)
    lw $s1, 4($a0)
    addi $s1, $s1, 2
    move $a0, $a1

output_loop_start:
    beq $s0, $zero, output_loop_end
    li $v0, PRINT_STRING
    syscall
    add $a0, $a0, $s1
    addi $s0, $s0, -1
    j output_loop_start
output_loop_end:

    lw $ra, 32($sp)
    lw $s7, 28($sp)
    lw $s6, 24($sp)
    lw $s5, 20($sp)
    lw $s4, 16($sp)
    lw $s3, 12($sp)
    lw $s2, 8($sp)
    lw $s1, 4($sp)
    lw $s0, 0($sp)
    addi $sp, $sp, 36

    jr $ra
