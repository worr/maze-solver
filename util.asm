.globl find_start
.globl copy_maze
.globl finish_solved

.globl pop

PERIOD = 46
START = 83
MAZE_SIZE = 80*80-1

.data

temp_address:
.word 0

.text

# Find the start of the maze
# $a0 - size of maze
# $a1 - maze to search
# $a2 - put the start of maze
find_start:
    addi $sp, $sp, -36
    sw $ra, 32($sp)
    sw $s7, 28($sp)
    sw $s6, 24($sp)
    sw $s5, 20($sp)
    sw $s4, 16($sp)
    sw $s3, 12($sp)
    sw $s2, 8($sp)
    sw $s1, 4($sp)
    sw $s0, 0($sp)

    li $s0, START
    lw $s1, 8($a0)
loop_start:
    lb $t0, 0($a1)
    beq $t0, $s0, loop_end
    addi $a1, $a1, 1
    j loop_start
loop_end:
    sw $a1, 0($a2)

    lw $ra, 32($sp)
    lw $s7, 28($sp)
    lw $s6, 24($sp)
    lw $s5, 20($sp)
    lw $s4, 16($sp)
    lw $s3, 12($sp)
    lw $s2, 8($sp)
    lw $s1, 4($sp)
    lw $s0, 0($sp)
    addi $sp, $sp, 36

    jr $ra

# This copies the clean maze into the solution
# $a0 - clean maze
# $a1 - solution maze
copy_maze:
    addi $sp, $sp, -36
    sw $ra, 32($sp)
    sw $s7, 28($sp)
    sw $s6, 24($sp)
    sw $s5, 20($sp)
    sw $s4, 16($sp)
    sw $s3, 12($sp)
    sw $s2, 8($sp)
    sw $s1, 4($sp)
    sw $s0, 0($sp)

    li $s0, MAZE_SIZE
    li $s1, 0

copy_maze_loop_start:
    lb $t0, 0($a0)
    sb $t0, 0($a1)
    addi $a0, $a0, 1
    addi $a1, $a1, 1
    addi $s1, 1
    bne $s1, $s0 , copy_maze_loop_start

    lw $ra, 32($sp)
    lw $s7, 28($sp)
    lw $s6, 24($sp)
    lw $s5, 20($sp)
    lw $s4, 16($sp)
    lw $s3, 12($sp)
    lw $s2, 8($sp)
    lw $s1, 4($sp)
    lw $s0, 0($sp)
    addi $sp, $sp, 36

    jr $ra

# This loads the solved maze with dots along the path
# $a0 - solved maze
# $a1 - clean solution maze
finish_solved:
    addi $sp, $sp, -36
    sw $ra, 32($sp)
    sw $s7, 28($sp)
    sw $s6, 24($sp)
    sw $s5, 20($sp)
    sw $s4, 16($sp)
    sw $s3, 12($sp)
    sw $s2, 8($sp)
    sw $s1, 4($sp)
    sw $s0, 0($sp)

# First we need to calculate an offset for the mem addresses
    sub $s0, $a1, $a0

# Now we pop until we can't pop no more and . some things
finish_solved_loop_start:
    la $a0, temp_address
    jal pop
    lw $t0, 0($a0) # Load the address from the stack
    beq $t0, $zero, finish_solved_loop_end

# Dot dot dot!
    li $t1, PERIOD
    add $t0, $t0, $s0 # Add the offset
    sb $t1, 0($t0)
    j finish_solved_loop_start

finish_solved_loop_end:

    lw $ra, 32($sp)
    lw $s7, 28($sp)
    lw $s6, 24($sp)
    lw $s5, 20($sp)
    lw $s4, 16($sp)
    lw $s3, 12($sp)
    lw $s2, 8($sp)
    lw $s1, 4($sp)
    lw $s0, 0($sp)
    addi $sp, $sp, 36

    jr $ra
