.globl check_direction
.globl check_decision
.globl move_mouse
.globl backtrack

.globl push
.globl pop

HASHTAG = 35 # Twitter has ruined me
PERIOD = 46
END = 69

.data

.text

# Checks direction to see if blocked
# $a0 - pointer to memory address of current location
# $a1 - direction to check
# $v0 - 0 if blocked
check_direction:
    addi $sp, $sp, -36
    sw $ra, 32($sp)
    sw $s7, 28($sp)
    sw $s6, 24($sp)
    sw $s5, 20($sp)
    sw $s4, 16($sp)
    sw $s3, 12($sp)
    sw $s2, 8($sp)
    sw $s1, 4($sp)
    sw $s0, 0($sp)

    lw $t0, 0($a0)
    add $s0, $t0, $a1   # Calculate new address to check
    lb $t0, 0($s0)      # Load the character at that address
    slti $v0, $t0, HASHTAG

    lw $ra, 32($sp)
    lw $s7, 28($sp)
    lw $s6, 24($sp)
    lw $s5, 20($sp)
    lw $s4, 16($sp)
    lw $s3, 12($sp)
    lw $s2, 8($sp)
    lw $s1, 4($sp)
    lw $s0, 0($sp)
    addi $sp, $sp, 36

    jr $ra

# Checks to see if there's an alternate path from this point
# Used in backtracking to figure out when to stop 
# $a0 - pointer to memory address to check
# $a1 - maze row length
# $v0 - 1 if decision point
check_decision:
    addi $sp, $sp, -36
    sw $ra, 32($sp)
    sw $s7, 28($sp)
    sw $s6, 24($sp)
    sw $s5, 20($sp)
    sw $s4, 16($sp)
    sw $s3, 12($sp)
    sw $s2, 8($sp)
    sw $s1, 4($sp)
    sw $s0, 0($sp)

    li $s0, 0 # This is where we're gonna store the answer
    li $s1, -1
    move $s2, $a1

# Check down
    jal check_direction
    or $s0, $s0, $v0

# Check up
    mul $a1, $s2, $s1
    jal check_direction
    or $s0, $s0, $v0

# Check left
    move $a1, $s1
    jal check_direction
    or $s0, $s0, $v0

# Check right
    mul $a1, $s1, $s1
    jal check_direction
    or $s0, $s0, $v0

    move $v0, $s0

    lw $ra, 32($sp)
    lw $s7, 28($sp)
    lw $s6, 24($sp)
    lw $s5, 20($sp)
    lw $s4, 16($sp)
    lw $s3, 12($sp)
    lw $s2, 8($sp)
    lw $s1, 4($sp)
    lw $s0, 0($sp)
    addi $sp, $sp, 36

    jr $ra

# Move! This changes the location of the current_loc pointer
# while also pushing to the stack and breadcrumbing
# $a0 - curent_loc pointer
# $a1 - new address to scoop in the current_loc pointer
move_mouse:
    addi $sp, $sp, -36
    sw $ra, 32($sp)
    sw $s7, 28($sp)
    sw $s6, 24($sp)
    sw $s5, 20($sp)
    sw $s4, 16($sp)
    sw $s3, 12($sp)
    sw $s2, 8($sp)
    sw $s1, 4($sp)
    sw $s0, 0($sp)

    sw $a1, 0($a0)
    jal push

# Let's breadcrumb this biznatch
    li $t0, PERIOD
    lw $t1, 0($a0)
    sb $t0, 0($t1)

    lw $ra, 32($sp)
    lw $s7, 28($sp)
    lw $s6, 24($sp)
    lw $s5, 20($sp)
    lw $s4, 16($sp)
    lw $s3, 12($sp)
    lw $s2, 8($sp)
    lw $s1, 4($sp)
    lw $s0, 0($sp)
    addi $sp, $sp, 36

    jr $ra

# This function backtracks to the last decision point. Really
# all it does is pop off the stack until a decision point has been
# reached.
# $a0 - current_loc pointer
# $a1 - size of maze row
backtrack:
    addi $sp, $sp, -36
    sw $ra, 32($sp)
    sw $s7, 28($sp)
    sw $s6, 24($sp)
    sw $s5, 20($sp)
    sw $s4, 16($sp)
    sw $s3, 12($sp)
    sw $s2, 8($sp)
    sw $s1, 4($sp)
    sw $s0, 0($sp)

    move $s0, $a0
    move $s1, $a1

backtrack_loop_start:
    jal pop
    move $a1, $s1
    jal check_decision
    beq $v0, $zero, backtrack_loop_start

# After loop, push the current location back onto the stack
    jal push

    lw $ra, 32($sp)
    lw $s7, 28($sp)
    lw $s6, 24($sp)
    lw $s5, 20($sp)
    lw $s4, 16($sp)
    lw $s3, 12($sp)
    lw $s2, 8($sp)
    lw $s1, 4($sp)
    lw $s0, 0($sp)
    addi $sp, $sp, 36

    jr $ra

# This function checks to see if any of the adjacent directions
# are the end of the maze
# $a0 - current_loc pointer
# $a1 - size of maze row
# $v0 - 1 if end detected (direction doesn't matter)
check_end:
    addi $sp, $sp, -36
    sw $ra, 32($sp)
    sw $s7, 28($sp)
    sw $s6, 24($sp)
    sw $s5, 20($sp)
    sw $s4, 16($sp)
    sw $s3, 12($sp)
    sw $s2, 8($sp)
    sw $s1, 4($sp)
    sw $s0, 0($sp)

    li $s0, -1
    move $s1, $a1
    li $s2, END

# Check left
    lw $t0, 0($a0)
    lb $t0, 1($t0)
    beq $s2, $t0, check_end_found

# Check right
    lw $t0, 0($a0)
    lb $t0, -1($t0)
    beq $s2, $t0, check_end_found

# Check down
    lw $t0, 0($a0)
    add $t0, $t0, $s1
    lb $t0, 0($t0)
    beq $s2, $t0, check_end_found

# Check up
    lw $t0, 0($a0)
    sub $t0, $t0, $s1
    lb $t0, 0($t0)
    beq $s2, $t0, check_end_found

    li $v0, 0
    j check_end_end

check_end_found:
    li $v0, 1

check_end_end:

    lw $ra, 32($sp)
    lw $s7, 28($sp)
    lw $s6, 24($sp)
    lw $s5, 20($sp)
    lw $s4, 16($sp)
    lw $s3, 12($sp)
    lw $s2, 8($sp)
    lw $s1, 4($sp)
    lw $s0, 0($sp)
    addi $sp, $sp, 36

    jr $ra
