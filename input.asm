# Let's declare constants!
READ_INT = 5
READ_STRING = 8

.text
.globl read_input

# Read input from stdin
# $a0 - address to put size of maze
# $a1 - address to maze
read_input:
    addi $sp, $sp, -36
    sw $ra, 32($sp)
    sw $s7, 28($sp)
    sw $s6, 24($sp)
    sw $s5, 20($sp)
    sw $s4, 16($sp)
    sw $s3, 12($sp)
    sw $s2, 8($sp)
    sw $s1, 4($sp)
    sw $s0, 0($sp)

    move $s0, $a0
    move $s1, $a1

# Let's read in size of maze!
    li $v0, READ_INT
    syscall
    sw $v0, 0($s0)
    move $s2, $v0 # We want this for later
    li $v0, READ_INT 
    syscall
    sw $v0, 4($s0)
    move $s3, $v0 # This too

# Now let's actually read in the maze!
    move $a0, $s1
    addi $s3, $s3, 2 # leave room for the NULL byte!
    sw $s3, 8($s0)
    move $a1, $s3
    
read_loop:
    beq $s2, $zero, read_loop_done
    li $v0, READ_STRING
    syscall
    add $a0, $a0, $s3
    addi $s2, $s2, -1
    j read_loop
read_loop_done:

    lw $ra, 32($sp)
    lw $s7, 28($sp)
    lw $s6, 24($sp)
    lw $s5, 20($sp)
    lw $s4, 16($sp)
    lw $s3, 12($sp)
    lw $s2, 8($sp)
    lw $s1, 4($sp)
    addi $sp, $sp, 36

    jr $ra
